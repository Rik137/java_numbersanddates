package practice;

import java.util.Scanner;

public class TrucksAndContainers {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //получение количество коробок от пользователя
        int boxes = scanner.nextInt();
        calcBoxes(boxes);

        // TODO: вывести в консоль коробки разложенные по грузовикам и контейнерам
        // пример вывода при вводе 2
        // для отступа используйте табуляцию - \t
        //  В каждый грузовик помещается максимум 12 контейнеров.
        // В каждый контейнер — не более 27 ящиков. Ящики, контейнеры и грузовики пронумерованы.

        /*
        Грузовик: 1
            Контейнер: 1
                Ящик: 1
                Ящик: 2
        Необходимо:
        грузовиков - 1 шт.
        контейнеров - 1 шт.
        */
    }
//------------------------------------------------------------------------
    public static void calcBoxes(int boxes) {
        int trucCount = 0;
        int containerCount = 0;
        int maxBoxCount = 27;
        int maxContainer = 12;

        for (int i = 1; i <= boxes; i++) {
            if (i == 1) {
                trucCount = 1;
                containerCount = 1;
                System.out.println(" Грузовик: " + trucCount + "\n" +
                        "\tКонтейнер: " + containerCount);
            }
            System.out.println("\t\tЯщик: " + i);
            if (i % maxBoxCount == 0 && boxes % maxBoxCount != 0) {
                containerCount++;
                if (i % (maxBoxCount * maxContainer) == 0) {
                    trucCount++;
                    System.out.println("Грузовик: " + trucCount);
                }
                System.out.println("\tКонтейнер: " + containerCount);
            }
        }
        System.out.println("Необходимо:" + "\n" +
                "грузовиков - " + trucCount + " шт." + "\n" +
                "контейнеров - " + containerCount + " шт.");
    }
}

