package practice;

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Locale;

public class Birthdays {
    public static void main(String[] args) {

        int day = 31;
        int month = 12;
        int year = 1990;

        System.out.println(collectBirthdays(year, month, day));

    }

    public static String collectBirthdays(int year, int month, int day) {
        LocalDate birthday = LocalDate.of(year, month, day);
        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy - EE").localizedBy(Locale.US);
        String massage = "";
        for (int i = 0; ; i++) {
            if (today.isBefore(birthday)) {
                break;
            }
            String text = i + " - ";
            massage = massage + text + formatter.format(birthday) + System.lineSeparator();
            birthday = birthday.plusYears(1);
        }
        return massage;
    }
}
